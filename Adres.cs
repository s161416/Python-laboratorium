﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cwiczenie_nowe
{
    class Adres
    {
        private string Miejscowosc;
        private string Ulica;
        private int Numer_domu;
        private int[] Kod_pocztowy = new int[2];

        public Adres()
        {
            Console.WriteLine("Działa automatycznie");
            Miejscowosc = "Skorzewo";
            Ulica = "Peplinskiego";
            Numer_domu = 53;
            Kod_pocztowy[0] = 83;
            Kod_pocztowy[1] = 400;

        }

        public Adres(string miejsc, string ulic, int numer, int kod1, int kod2)
        {
            Console.WriteLine("Działa z wpisaniem");
            Miejscowosc = miejsc;
            Ulica = ulic;
            Numer_domu = numer;
            Kod_pocztowy[0] = kod1;
            Kod_pocztowy[1] = kod2;
        }

        public string getTown() { return Miejscowosc;  }
        public string getStreet() { return Ulica; }
        public int getHouseNumber() { return Numer_domu; }
        public int[] getCode() { return Kod_pocztowy; }


        public void setTown(string miejsc) { Miejscowosc = miejsc; }
        public void setStreet(string ulic) { Ulica = ulic; }
        public void setHouseNumber(int numer) { Numer_domu = numer; }
        public void setCode(int kod1, int kod2) { Kod_pocztowy[0] = kod1;  Kod_pocztowy[1] = kod2; }
        
    }
}
