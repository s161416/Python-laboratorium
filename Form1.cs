﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace lalala
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void oblicz_Click(object sender, EventArgs e)
        {
            if (dodawanie.Checked == true)
            {
                label1.Text = (System.Double.Parse(textBox1.Text) + System.Double.Parse(textBox2.Text)).ToString();
            }
            if (odejmowanie.Checked == true)
            {
                label1.Text = (System.Double.Parse(textBox1.Text) - System.Double.Parse(textBox2.Text)).ToString();
            }
            if (mnozenie.Checked == true)
            {
                label1.Text = (System.Double.Parse(textBox1.Text) * System.Double.Parse(textBox2.Text)).ToString();
            }
            if (dzielenie.Checked == true)
            {
                try
                {
                    label1.Text = (System.Double.Parse(textBox1.Text) / System.Double.Parse(textBox2.Text)).ToString();
                }
                catch {
                    label1.Text = "złe dane";
                }
            }
        }

        private void zamknij_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            label2.Text = monthCalendar1.SelectionRange.Start.ToShortDateString();

        }
        
       
    }
}
