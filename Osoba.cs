﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cwiczenie_nowe
{
    class Osoba
    {
        private string Imie;
        private string Nazwisko;
        private Adres adres_zamieszkania;

        public Osoba()
        {
            Imie = "Zuzanna";
            Nazwisko = "Bazychowska";
            adres_zamieszkania = new Adres();

        }

        public Osoba(string imie, string nazwisko, Adres adres_zam)
        {
            Imie = imie;
            Nazwisko = nazwisko;
            adres_zamieszkania = adres_zam;
        }

        public string getName() { return Imie; }
        public string getSurname() { return Nazwisko; }
        public Adres getAdres() { return adres_zamieszkania; }

        public void setName(string imie) { Imie = imie; }
        public void setSurname(string nazwisko) { Nazwisko = nazwisko; }
        public void setAdres(Adres adres_zam) { adres_zamieszkania = adres_zam; }
    }
}
