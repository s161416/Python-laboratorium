﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cwiczenie_nowe
{
    class Program
    {
        static void Main(string[] args)
        {
           Adres adres_zamieszkania = new cwiczenie_nowe.Adres();
            Console.WriteLine("Miasto:" + adres_zamieszkania.getTown());
            Console.WriteLine("Ulica:" + adres_zamieszkania.getStreet());
            Console.WriteLine("Numer domu:" + adres_zamieszkania.getHouseNumber());
            Console.WriteLine("Kod pocztowy:" + adres_zamieszkania.getCode()[0] + "-" + adres_zamieszkania.getCode()[1] );

            Console.WriteLine("------------------------");

            Adres nowy_adres = new cwiczenie_nowe.Adres("Gdansk", "Slonimskiego" , 34,83,400);
            Console.WriteLine("Nowe miasto:" + nowy_adres.getTown());
            Console.WriteLine("Nowa ulica:" + nowy_adres.getStreet());
            Console.WriteLine("Nowy numer:" + nowy_adres.getHouseNumber());
            Console.WriteLine("Nowy kod:" + nowy_adres.getCode()[0] + "-" + nowy_adres.getCode()[1]);

            Console.WriteLine("------------------------");

            adres_zamieszkania.setTown("Koscierzyna");
            adres_zamieszkania.setStreet("Komorowskiego");
            adres_zamieszkania.setHouseNumber(23);
            adres_zamieszkania.setCode(34, 678);

            Console.WriteLine("Miasto:" + adres_zamieszkania.getTown());
            Console.WriteLine("Ulica:" + adres_zamieszkania.getStreet());
            Console.WriteLine("Numer domu:" + adres_zamieszkania.getHouseNumber());
            Console.WriteLine("Kod pocztowy:" + adres_zamieszkania.getCode()[0] + "-" + adres_zamieszkania.getCode()[1]);

            Console.WriteLine("------------------------");

            Osoba dane_osobowe = new cwiczenie_nowe.Osoba();
            Console.WriteLine("Imię to:" + dane_osobowe.getName());
            Console.WriteLine("Nazwisko to:" + dane_osobowe.getSurname());
            Console.WriteLine("Adres zamieszkania to:" + dane_osobowe.getAdres().getTown() + "  " + dane_osobowe.getAdres().getStreet() + dane_osobowe.getAdres().getHouseNumber() );

            Console.WriteLine("------------------------");

            Osoba nowe_dane = new cwiczenie_nowe.Osoba("Ania", "Mańska", nowy_adres);
            Console.WriteLine("Nowe imię:" + nowe_dane.getName());
            Console.WriteLine("Nowe nazwisko:" + nowe_dane.getSurname());
            Console.WriteLine("Nowy adres zamieszkania:" + nowe_dane.getAdres().getTown() + nowe_dane.getAdres().getStreet());

            dane_osobowe.setName("Michał");
            dane_osobowe.setSurname("Zarach");
            dane_osobowe.setAdres(new Adres());

            Console.WriteLine("Imię:" + dane_osobowe.getName());
            Console.WriteLine("Nazwisko" + dane_osobowe.getSurname());
            Console.WriteLine("Nowy adres:" + dane_osobowe.getAdres().getTown());

            Console.ReadKey();
        }
    }
}
